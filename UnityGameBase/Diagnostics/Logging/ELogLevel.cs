using System;
namespace UGB.Diagnostics.Logging
{
	public enum ELogLevel
	{
		debug = 0,
		warning,
		error,
		exception
	}
}