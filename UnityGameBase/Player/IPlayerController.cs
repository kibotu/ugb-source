using System;
using UnityEngine;

namespace UGB.Player
{
	public interface IPlayerController
	{
		Transform transform {get;}
	}
}